package mx.rpedrazacoello.data_structures.Heap.Heapify;

import mx.rpedrazacoello.data_structures.Heap.HeapInterface.HeapInterface;

/**
 * LECTURE VIDEOS:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/lecture-4-heaps-and-heap-sort/
 *
 * LECTURE NOTES:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/MIT6_006F11_lec04.pdf
 *
 * FOR THE SENSE OF SIMPLICITY THE NODES CAN HAVE, AT MOST, A KEY VALUE OF 99. ALSO THE NODES CAN HAVE, AT LEAST,
 * A KEY VALUE OF 0. KEY_VALUE=100 REPRESENTS INFINITY, AND KEY_VALUE=-1 REPRESENTS MINUS INFINITY
 */
abstract class Heapify implements HeapInterface {

    protected HeapNode root;
    protected int count=0;
    protected static final int INFINITY = 100;
    protected static final int MINUS_INFINITY=-1;

    public Heapify(){
    }

    public Heapify(HeapNode root) {
        this.root = root;
        count++;
    }

    /**
     * Method that adds a HeapNode to the Heap.
     * This methods is in charge of fixing the Heap so that the heap-invariant is fulfilled
     * @param added
     */
    @Override
    public void addToHeap(HeapNode added){
        add(added);
        heapifyUp(added);
    }

    /**
     * Method that prints the nodes sorted.
     * If the heap is a min-heap it's printed Min to Max
     * If the heap is a max-heap it's printed Max to Min
     */
    public void printOrder(){
        System.out.println("Heap: ");
        HeapNode temp;
        while(root!=null){
            temp = this.extractRootFromHeap();
            System.out.print(temp.getValue() +", ");
        }
    }

    /**
     * This method swaps the position of two nodes.
     * @param node1
     * @param node2
     */
    protected void swap(HeapNode node1, HeapNode node2) {

        if(node1.isSon(node2)){
            swapFatherSon(node1, node2);
        } else if(node2.isSon(node1)){
            swapFatherSon(node2, node1);
        } else {
            if (node1.getParent() != null) {
                ((HeapNode)node1.getParent()).changeSon(node1, node2);
            }

            if (node2.getParent() != null) {
                ((HeapNode)node2.getParent()).changeSon(node2, node1);
            }

            HeapNode temp;

            temp = (HeapNode)node1.getParent();
            node1.setParent(node2.getParent());
            node2.setParent(temp);

            temp = (HeapNode)node1.getLeftSon();
            node1.setLeftSon(node2.getLeftSon());
            node2.setLeftSon(temp);

            temp = (HeapNode)node1.getRightSon();
            node1.setRightSon(node2.getRightSon());
            node2.setRightSon(temp);

            fixPointersSonsToNode(node1);
            fixPointersSonsToNode(node2);
        }

        //If after swapping there's a new root
        if(node1.isRoot()){
            this.root=node1;
        } else if(node2.isRoot()){
            this.root=node2;
        }
    }

    /**
     * This methods receives a node called temporary root.
     * If the Heap is a Min-Heap then the temporary root has to have a key value of INFINITY.
     * If the Heap is a Max-Heap then the temporary root has to have a key value of MINUS-INFINITY.
     * @param temporaryRoot
     * @return
     */
     protected HeapNode extractRootFromHeap(HeapNode temporaryRoot){
        HeapNode delete = root;

        add(temporaryRoot);
        // Now, we swap the delete node with temporaryRoot
        swap(delete, temporaryRoot);
        //By doing this, now we are able to delete the node from the heap and keep the complete-binary tree form
        deleteLeaf(delete);
        //The last thing we need to do is to heapify down the temporary node all the way to the leafs
        //So that can be deleted too.
        heapifyDown(temporaryRoot);
        deleteLeaf(temporaryRoot);

        return delete;
    }

    /**
     * Method that deletes a Leaf
     * @param leaf
     */
    private void deleteLeaf(HeapNode leaf){
        //It's not a leaf
        if(leaf.getLeftSon()!= null || leaf.getRightSon()!=null){
            throw new UnsupportedOperationException();
        }

        if(root==leaf){
            root=null;
        } else {
            ((HeapNode)leaf.getParent()).changeSon(leaf, null);
        }
        count--;
    }

    /**
     * This methods adds a Node to the Heap.
     * The node is added so that the heap-tree continues to be a complete-binary tree.
     * @param added
     */
    private void add(HeapNode added){
        if(root==null){
            root = added;
            return;
        }
        HeapNode current = root;
        count++;
        int height = (int)(Math.log(count)/Math.log(2));
        int start = (int)Math.pow(2, height);
        int finish = (int)Math.pow(2, height+1) -1;
        while(true){
            float middleFloat = ((float)start + (float)finish) / 2.0f;
            if(count < middleFloat){
                if(current.getLeftSon()!=null){
                    current = (HeapNode)current.getLeftSon();
                    finish = (int)middleFloat; //Round down
                } else {
                    break;
                }
            } else if (count > middleFloat){
                if(current.getRightSon()!=null){
                    current = (HeapNode) current.getRightSon();
                    start= (int)Math.ceil(middleFloat); //Round up
                } else {
                    break;
                }
            } else {
                break;
            }
        }
        addSon(current, added);
    }

    /**
     * Method that receives a soon-to-be parent node and a soon-to-be son node.
     * This method sets the parent-son and son-parent pointers.
     *
     * If the parent alredy have two sons then it throws and Exception
     * @param parent
     * @param son
     */
    private void addSon(HeapNode parent, HeapNode son){
        if(parent.getLeftSon()==null){
            parent.setLeftSon(son);
        } else if(parent.getRightSon()==null){
            parent.setRightSon(son);
        } else {
            throw new UnsupportedOperationException();
        }
        son.setParent(parent);
    }

    /**
     * Recomendation: Do not call this method directly, it's better to call the swap method.
     * This method swaps a father-son position in a Heap.
     * @param parent
     * @param son
     */
    private void swapFatherSon(HeapNode parent, HeapNode son){
        if(parent.getParent()!=null){
            ((HeapNode)parent.getParent()).changeSon(parent, son);
        }
        son.setParent(parent.getParent());
        parent.setParent(son);

        HeapNode temp;
        if(parent.getLeftSon()==son){
            parent.setLeftSon(son.getLeftSon());
            temp = (HeapNode)parent.getRightSon();
            parent.setRightSon(son.getRightSon());
            son.setLeftSon(parent);
            son.setRightSon(temp);
        } else if (parent.getRightSon()==son){
            parent.setRightSon(son.getRightSon());
            temp = (HeapNode)parent.getLeftSon();
            parent.setLeftSon(son.getLeftSon());
            son.setRightSon(parent);
            son.setLeftSon(temp);
        } else {
            throw new UnsupportedOperationException();
        }

        fixPointersSonsToNode(parent);
        fixPointersSonsToNode(son);
    }

    /**
     * Fix the pointers so that the sons of the node n are pointing to the node n as their parent
     * @param node
     */
    private void fixPointersSonsToNode(HeapNode node){
        if(node.getRightSon()!=null) {
            ((HeapNode)node.getRightSon()).setParent(node);
        }

        if(node.getLeftSon()!=null) {
            ((HeapNode)node.getLeftSon()).setParent(node);
        }
    }

    /**
     * Methods that extracts the root from the heap
     * @return extracted root of the heap
     */
    public abstract HeapNode extractRootFromHeap();

    abstract void heapifyDown(HeapNode node);

    abstract void heapifyUp(HeapNode node);
}
