package mx.rpedrazacoello.data_structures.Heap.Heapify;


import mx.rpedrazacoello.data_structures.BinaryNode;

/**
 * LECTURE VIDEOS:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/lecture-4-heaps-and-heap-sort/
 *
 * LECTURE NOTES:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/MIT6_006F11_lec04.pdf
 */
public final class HeapNode extends BinaryNode {


    private int key;
    private Object value;

    public HeapNode(int key, Object value) {
        this.key = key;
        this.value = value;
    }

    /**
     * Method that swaps a HeapNode's son with another HeapNode
     * Takes care of the parent-son pointers.
     * THE POINTERS FROM THE SON TO THE PARENT ARE NOT CHANGED.
     * @param oldSon
     * @param newSon
     */
    void changeSon(HeapNode oldSon, HeapNode newSon){
        if(leftSon == oldSon){
            leftSon=newSon;
        } else if (rightSon==oldSon){
            rightSon=newSon;
        } else {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * True if @param node is a son of this.node
     * False otherwise
     * @param node
     * @return
     */
    boolean isSon(HeapNode node){
        if(node != null){
            if(leftSon != null){
                if (leftSon==node){
                    return true;
                }
            }

            if (rightSon != null){
                if(rightSon==node){
                    return true;
                }
            }
        }
        return false;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    protected BinaryNode getParent() {
        return parent;
    }

    @Override
    protected void setLeftSon(BinaryNode leftSon) {
        this.leftSon = leftSon;
    }

    @Override
    protected void setRightSon(BinaryNode rightSon) {
        this.rightSon = rightSon;
    }
}
