package mx.rpedrazacoello.data_structures.Heap.Heapify;

/**
 * LECTURE VIDEOS:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/lecture-4-heaps-and-heap-sort/
 *
 * LECTURE NOTES:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/MIT6_006F11_lec04.pdf
 *
 * FOR THE SENSE OF SIMPLICITY THE NODES CAN HAVE, AT MOST, A KEY VALUE OF 99. ALSO THE NODES CAN HAVE, AT LEAST,
 * A KEY VALUE OF 0. KEY_VALUE=100 REPRESENTS INFINITY, AND KEY_VALUE=-1 REPRESENTS MINUS INFINITY
 */
public final class MinHeapify extends Heapify{

    public MinHeapify(){

    }

    /**
     * Constructor Method.
     * The param becomes the root of the heap
     * @param root
     */
    public MinHeapify(HeapNode root) {
        super(root);
    }

    /**
     * Receives the reference of a node that exists inside the heap. The node has to have an updated key value,
     * otherwise the heap wont change.
     * @param node
     */
    @Override
    public void updateFromHeap(HeapNode node){
        if(node.getParent()!=null){
            if(node.getKey()<((HeapNode)node.getParent()).getKey()){
                heapifyUp(node);
                return;
            }
        }

        if(node.getLeftSon()!=null || node.getRightSon() != null){
            if(node.getLeftSon()!=null && node.getRightSon() != null){
                if(((HeapNode)node.getLeftSon()).getKey()<node.getKey() ||
                        ((HeapNode)node.getRightSon()).getKey()<node.getKey()){
                    heapifyDown(node);
                }
            } else {
                if(node.getLeftSon()!=null){
                    if(((HeapNode)node.getLeftSon()).getKey()<node.getKey()){
                        heapifyDown(node);
                    }
                } else if (node.getRightSon()!=null){
                    if (((HeapNode)node.getRightSon()).getKey()<node.getKey()){
                        heapifyDown(node);
                    }
                }
            }
        }

    }

    /**
     * This method receives as Params a node, that belongs to the heap, that you want to remove.
     * @param node
     */
    @Override
    public void removeFromHeap(HeapNode node){
        //The first thing we'll do is make this node, the root of the tree.
        node.setKey(MINUS_INFINITY);
        heapifyUp(node);

        //Know that the node is the root we'll extract the root from the heap
        extractRootFromHeap();
    }

    /**
     * Method that extracts the root from the heap
     * @return extracted root
     */
    @Override
    public HeapNode extractRootFromHeap() {
        //Add a new node with key_value = infinity
        HeapNode temporaryRoot = new HeapNode(INFINITY, "Temporary root");
        return extractRootFromHeap(temporaryRoot);
    }

    /**
     * Method that gets the root from the heap
     * @return root of the heap
     */
    @Override
    public HeapNode getRoot() {
        return super.root;
    }

    /**
     * Method that receives a node and fix the Heap invariant from that node towards the root.
     * The descendants of the node are not touched.
     * @param node
     */
    void heapifyUp(HeapNode node) {
        while(true){
            if(node.getParent()!=null) {
                if (((HeapNode)node.getParent()).getKey() > node.getKey()) {
                    swap((HeapNode)node.getParent(), node);
                } else {
                    break;
                }
            } else {
                break;
            }
        }
    }

    /**
     * Method that receives a node and fix the Heap invariant from that node towards the left.
     * The ascendants of the node are not touched.
     * @param node
     */
    void heapifyDown(HeapNode node) {
        while(true){
            if(node.getRightSon()!=null || node.getLeftSon()!=null) {
                int leftSonKey=INFINITY, rightSonKey=INFINITY;
                if(node.getLeftSon()!=null){
                    leftSonKey=((HeapNode)node.getLeftSon()).getKey();
                }

                if(node.getRightSon()!=null){
                    rightSonKey=((HeapNode)node.getRightSon()).getKey();
                }

                if(node.getKey()>leftSonKey || node.getKey()>rightSonKey){
                    if(leftSonKey<rightSonKey){
                        swap((HeapNode)node.getLeftSon(), node);
                    } else {
                        swap((HeapNode)node.getRightSon(), node);
                    }
                } else {
                    break;
                }
            } else {
                break;
            }
        }
    }
}
