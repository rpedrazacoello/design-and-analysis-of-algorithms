package mx.rpedrazacoello.data_structures.Heap.HeapInterface;


import mx.rpedrazacoello.data_structures.Heap.Heapify.HeapNode;

/**
 * All the functionality that the heap should include
 *
 * LECTURE VIDEOS:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/lecture-4-heaps-and-heap-sort/
 *
 * LECTURE NOTES:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/MIT6_006F11_lec04.pdf
 */
public interface HeapInterface {

    /**
     * Add a node to the heap.
     * The heaps is in charge of fixing the heap-invariant.
     * @param heapNode
     */
    void addToHeap(HeapNode heapNode);

    /**
     * Method that receives an updated node (alredy in the heap) and fixes the heap invariant.
     * @param node
     */
    void updateFromHeap(HeapNode node);

    /**
     * Method that removes a node from the heap and fixes the heap invariant.
     * @param node
     */
    void removeFromHeap(HeapNode node);

    /**
     * Method that extracts root from the heap. Fixes the heap invariant
     * @return
     */
    HeapNode extractRootFromHeap();

    /**
     * Method that returns the root of the heap.
     * @return
     */
    HeapNode getRoot();
}
