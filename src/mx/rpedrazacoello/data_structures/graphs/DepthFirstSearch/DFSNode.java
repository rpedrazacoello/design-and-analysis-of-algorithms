package mx.rpedrazacoello.data_structures.graphs.DepthFirstSearch;


import mx.rpedrazacoello.data_structures.graphs.Node;

/**
 * LECTURE VIDEO:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/lecture-14-depth-first-search-dfs-topological-sort/
 *
 * LECTURE NOTES:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/MIT6_006F11_lec14.pdf
 *
 * EXTRA READING TO UNDERSTAND TOPOLOGICAL SORT
 * Introduction to Algorithms, Cormen
 */
public class DFSNode extends Node {

    private final static int DEFAULT_TIME = -1;
    private int startTime = DEFAULT_TIME;
    private int finishTime = DEFAULT_TIME;

    // phase = 1 if the Edge has not been visited (before startTime)
    // phase = 2 if the Edge is being visited (between startTime and finishTime)
    // phase = 3 if the Edge has been completely visited (after finishTime)
    private int phase;
    public static final int PHASE_1 = 1;
    public static final int PHASE_2 = 2;
    public static final int PHASE_3 = 3;

    /**
     * Constructor method.
     * Initializes the phase = 1;
     * @param identifier
     */
    public DFSNode(String identifier) {
        super(identifier);
        phase=1;
    }

    /**
     * Method that returns the start time of the node
     * @return
     */
    public int getStartTime() {
        return startTime;
    }

    /**
     * Method that sets the start time of the node.
     * You can only set start time once.
     * @param startTime
     */
    public void setStartTime(int startTime) {
        if(this.startTime == DEFAULT_TIME) {
            this.startTime = startTime;
        }
    }

    /**
     * Method that gets the finish time
     * @return
     */
    public int getFinishTime() {
        return finishTime;
    }

    /**
     * Method that sets the finish time of the node.
     * You can only set finish time once.
     * @param finishTime
     */
    public void setFinishTime(int finishTime) {
        if(this.finishTime == DEFAULT_TIME) {
            this.finishTime = finishTime;
        }
    }

    /**
     * The phase = 1 when the edge is created.
     * When the edge is visited, the phase = 2.
     * When the edge has been completely visited, the phase = 3
     */
    public void advancePhase(){
        if(phase<PHASE_3){
            phase++;
        }
    }

    /**
     * Method that return the phase of a node.
     * The phase = 1 when the edge is created.
     * When the edge is visited, the phase = 2.
     * When the edge has been completely visited, the phase = 3
     * @return
     */
    public int getPhase(){
        return phase;
    }
}
