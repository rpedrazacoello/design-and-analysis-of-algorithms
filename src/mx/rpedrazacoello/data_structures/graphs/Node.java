package mx.rpedrazacoello.data_structures.graphs;

import java.util.Collection;
import java.util.Hashtable;
import java.util.Objects;

public class Node {

    private String identifier;
    private Hashtable<Edge, Edge> edges;

    /**
     * Constructor method
     * @param identifier
     */
    public Node(String identifier) {
        this.identifier = identifier;
        edges = new Hashtable<>();
    }

    /**
     * Method that adds an Edge to the node.
     * The edge is only added if node is contained in the edge.
     * @param edge
     */
    public void addEdge(Edge edge){
        if(edge.getNodeConnectedTo(this)!=null) {
            edges.put(edge, edge);
        }
    }

    /**
     * Returns the identifier of the node
     * @return
     */
    public String getIdentifier() {
        return identifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Node)) return false;
        Node node = (Node) o;
        return Objects.equals(identifier, node.identifier);
    }

    /**
     * Method that returns the edges of the node
     * @return
     */
    public Collection<Edge> getEdges(){
        return edges.values();
    }
}
