package mx.rpedrazacoello.data_structures.graphs.WeightGraph;


import mx.rpedrazacoello.data_structures.graphs.Node;

public class WeightNode extends Node {

    private int distance;
    private boolean infiniteDistance = true;
    private String parentIdentifier = null;
    private boolean undetermined;

    /**
     * Constructor of the WeightNode
     * @param identifier
     */
    public WeightNode(String identifier) {
        super(identifier);
    }

    /**
     * Returns the current shortest distance from root to this node.
     * @return
     */
    public int getDistance() {
        return distance;
    }

    /**
     * Returns true if there is no known distance from root to this node is.
     * Returns false otherwise.
     * @return
     */
    public boolean isInfiniteDistance() {
        return infiniteDistance;
    }

    /**
     * Sets the distance if newDistance < currentDistance.
     * Returns true if newDistance < currentDistance.
     * Return false otherwise.
     * @param newDistance
     * @return
     */
    public boolean ifNewDistanceLess_setNewDistance(int newDistance){
        if(infiniteDistance || newDistance<this.distance){
            this.infiniteDistance=false;
            this.distance = newDistance;
            return true;
        }

        return false;
    }

    /**
     * If there's a current shortest path from root to v it looks this way = Path(root, u) + Edge(u,v).
     * In this case, the method would return the u node identifier.
     * @return
     */
    public String getParentIdentifier() {
        return parentIdentifier;
    }

    /**
     * If there's a new current shortest path from root to v it looks this way = Path(root, u) + Edge(u,v).
     * In this case, this method would set a new parent identifier.
     */
    public void setParentIdentifier(String parentIdentifier) {
        this.parentIdentifier = parentIdentifier;
    }

    public boolean isUndetermined() {
        return undetermined;
    }

    public void setUndetermined(boolean undetermined) {
        this.undetermined = undetermined;
    }
}
