package mx.rpedrazacoello.data_structures;

/**
 * Abstract class of a binary node
 */
public abstract class BinaryNode {

    protected BinaryNode parent;
    protected BinaryNode leftSon;
    protected BinaryNode rightSon;

    /**
     * Return true if the node parent pointer is null. False otherwise.
     * @return
     */
    public boolean isRoot() {
        return parent==null;
    }

    /**
     * Method that returns the parent of the node
     * @return
     */
    protected abstract BinaryNode getParent();

    /**
     * Method that sets the parent of the node
     * @param parent
     */
    public void setParent(BinaryNode parent){
        this.parent=parent;
    }

    /**
     * Method that gets the left son of the node
     * @return
     */
    public BinaryNode getLeftSon(){
        return leftSon;
    }

    /**
     * Method that gets the right son of the node
     * @param leftSon
     */
    protected abstract void setLeftSon(BinaryNode leftSon);

    /**
     * Method that gets the right son of the node
     * @return
     */
    public BinaryNode getRightSon(){
        return rightSon;
    }

    /**
     * Method that sets the right son of the node
     * @param rightSon
     */
    protected abstract void setRightSon(BinaryNode rightSon);
}
