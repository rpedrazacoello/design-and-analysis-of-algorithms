package mx.rpedrazacoello.greedy.restaurant_location;

import mx.rpedrazacoello.greedy.restaurant_location.util.Edge;
import mx.rpedrazacoello.greedy.restaurant_location.util.Node;

import java.util.*;

/**
 * PROBLEM OBTAINED FROM:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-046j-design-and-analysis-of-algorithms-spring-2015/assignments/MIT6_046JS15_pset1.pdf
 *
 * Drunken Donuts, a new wine-and-donuts restaurant chain, wants to build restaurants on many street corners with the
 * goal of maximizing their total profit. The street network is described as an undirected graph G = (V, E), where the
 * potential restaurant sites are the vertices of the graph. Each vertex u has a nonnegative integer value pu, which
 * describes the potential profit of site u. Two restaurants cannot be built on adjacent vertices (to avoid
 * selfcompetition). You are supposed to design an algorithm that outputs the chosen set U ⊆ V of sites that maximizes
 * the total profit u∈U sum(pu).
 *
 * First, for parts (1)–(3), suppose that the street network G is acyclic, i.e., a tree.
 *
 * 1. Consider the following “greedy” restaurant-placement algorithm: Choose the highest- profit vertex u0 in the tree
 * (breaking ties according to some order on vertex names) and put it into U. Remove u0 from further consideration,
 * along with all of its neighbors in G. Repeat until no further vertices remain. Give a counterexample to show that
 * this algorithm does not always give a restaurant placement with the maximum profit.
 */
public class RestaurantLocation {

    private static PriorityQueue<Node> maxHeap;
    private static ArrayList<Node> selectedNodes;

    public static void main (String [] args){
        maxHeap = new PriorityQueue<>(10);
        selectedNodes = new ArrayList<>();
        fillPriorityQueue(); //Counterexample of why this algorithm is not correct
        selectLocations();
        printResults();
    }

    private static void printResults(){
        System.out.println("Selected Locations: ");
        int sum = 0;
        for (int i=0; i<selectedNodes.size(); i++) {
            sum = sum + selectedNodes.get(i).getProfit();
            if(i==selectedNodes.size()-1){
                System.out.print(selectedNodes.get(i).getIdentifier());
            } else {
                System.out.println(selectedNodes.get(i).getIdentifier() + ", ");
            }
        }
        System.out.println("\nMax Profit: " +sum);
    }

    private static void selectLocations(){
        Iterator<Node> iterator = maxHeap.iterator();
        while (iterator.hasNext()){
            Node node = iterator.next();
            if(node.isActive()){
                selectedNodes.add(node);
                node.deactivateNeighbors();
            }
        }
    }

    private static void fillPriorityQueue(){
        Node A = new Node("A", 30);
        Node B = new Node ("B", 11);
        Node C = new Node ("C", 11);
        Node D = new Node ("D", 11);

        Edge edge1 = new Edge(A, B);
        Edge edge2 = new Edge(A, C);
        Edge edge3 = new Edge(A, D);

        A.addEdge(edge1);
        A.addEdge(edge2);
        A.addEdge(edge3);

        B.addEdge(edge1);
        C.addEdge(edge2);
        D.addEdge(edge3);

        maxHeap.add(A);
        maxHeap.add(B);
        maxHeap.add(C);
        maxHeap.add(D);
    }
}
