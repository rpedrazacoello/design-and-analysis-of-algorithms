package mx.rpedrazacoello.greedy.restaurant_location;

import mx.rpedrazacoello.data_structures.Heap.Heapify.HeapNode;
import mx.rpedrazacoello.data_structures.Heap.Heapify.MinHeapify;
import mx.rpedrazacoello.greedy.restaurant_location.util.Edge;
import mx.rpedrazacoello.greedy.restaurant_location.util.Node;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

/**
 * PROBLEM OBTAINED FROM:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-046j-design-and-analysis-of-algorithms-spring-2015/assignments/MIT6_046JS15_pset1.pdf
 *
 * Drunken Donuts, a new wine-and-donuts restaurant chain, wants to build restaurants on many street corners with the
 * goal of maximizing their total profit. The street network is described as an undirected graph G = (V, E), where the
 * potential restaurant sites are the vertices of the graph. Each vertex u has a nonnegative integer value pu, which
 * describes the potential profit of site u. Two restaurants cannot be built on adjacent vertices (to avoid
 * selfcompetition). You are supposed to design an algorithm that outputs the chosen set U ⊆ V of sites that maximizes
 * the total profit u∈U sum(pu).
 *
 * First, for parts (1)–(3), suppose that the street network G is acyclic, i.e., a tree.
 *
 * 3. Suppose that, in the absence of good market research, DD decides that all sites are equally good, so the goal is
 * simply to design a restaurant placement with the largest number of locations. Give a simple greedy algorithm for this
 * case, and prove its correctness.
 *
 * Claim: By choosing the locations with the minimum number of active neighbors you maximize the number of locations.
 *
 * Algorithm:
 * 1. Mark every location as "active"
 * 2. Create a min-heap with node as a value, and the number active of neighbors of that node as key.
 * 3. minNeighbors = extract root of the tree. This is a selected location.
 * 4. S = Set of neighbors of minNeighbors.
 * 5. SS = Set of neighbors of at least one location in S (Neighbors of the neighbors of minNeighbors)
 * 6. Mark every node in S as unactive
 * 7. Modify the min-heap key of every node that belongs to SS (newKey = lastKey -1).
 * 8. Update the min-heap with the new keys
 * 9. Go back to step number 3 until min-heap is empty.
 */
public class RestaurantLocation_SecondVersion {

    private static ArrayList<Node> nodes;
    private static MinHeapify minHeap;
    private static Hashtable<Node, HeapNode> heapNodeHashtable;
    private static ArrayList<Node> selectedNodes;

    public static void main (String [] args){
        nodes = new ArrayList<>();
        minHeap = new MinHeapify();
        heapNodeHashtable = new Hashtable<>();
        selectedNodes = new ArrayList<>();
        fill();
        fillMinHeap();
        selectLocation();
        printResults();
    }

    private static void selectLocation(){ //O(n^3 lgn)
        while (minHeap.getRoot()!=null) {
            //Select the active node with less active neighbors - O(lgn)
            HeapNode heapNode = minHeap.extractRootFromHeap();
            Node node = (Node) heapNode.getValue();

            if (node.isActive()) { //If the node is active
                selectedNodes.add(node);
                node.setActive(false);
                Iterator neighbors = node.getEdges().iterator();

                while (neighbors.hasNext()) { //Deactivate every neighbor of the node if active
                    Node neighbor = ((Edge) neighbors.next()).getNodeConnectedTo(node);
                    if (neighbor.isActive()) {
                        neighbor.setActive(false);
                        Iterator iterator = neighbor.getEdges().iterator();
                        while (iterator.hasNext()) {
                            //Change the key value of the neighbors of the neighbors of the node, update the min-heap
                            Node temp = ((Edge) iterator.next()).getNodeConnectedTo(neighbor);
                            if(temp.isActive()) {
                                HeapNode hn = heapNodeHashtable.get(temp);
                                hn.setKey(hn.getKey() - 1);
                                minHeap.updateFromHeap(hn); //O(lgn)
                            }
                        }
                    }
                }
            }
        }
    }

    private static void fillMinHeap(){
        for (Node node: nodes) {
            int numberNeighbors = node.getEdges().size();
            HeapNode heapNode = new HeapNode(numberNeighbors, node);
            minHeap.addToHeap(heapNode);
            heapNodeHashtable.put(node, heapNode);
        }
    }

    private static void fill(){
        Node A = new Node ("A", 1);
        Node B = new Node ("B", 1);
        Node C = new Node ("C", 1);
        Node D = new Node ("D", 1);
        Node E = new Node ("E", 1);
        Node F = new Node ("F", 1);
        Node G = new Node ("G", 1);
        Node H = new Node ("H", 1);
        Node I = new Node ("I", 1);
        Node J = new Node ("J", 1);

        Edge edge1 = new Edge(A, C);
        A.addEdge(edge1);
        C.addEdge(edge1);

        Edge edge2 = new Edge(B, C);
        B.addEdge(edge2);
        C.addEdge(edge2);

        Edge edge3 = new Edge(B, D);
        B.addEdge(edge3);
        D.addEdge(edge3);

        Edge edge4 = new Edge(C, D);
        C.addEdge(edge4);
        D.addEdge(edge4);

        Edge edge5 = new Edge(D, E);
        D.addEdge(edge5);
        E.addEdge(edge5);

        Edge edge6 = new Edge(E, F);
        E.addEdge(edge6);
        F.addEdge(edge6);

        Edge edge7 = new Edge(E, G);
        E.addEdge(edge7);
        G.addEdge(edge7);

        Edge edge8 = new Edge(F, G);
        F.addEdge(edge8);
        G.addEdge(edge8);

        Edge edge9 = new Edge(G, J);
        G.addEdge(edge9);
        J.addEdge(edge9);

        Edge edge10 = new Edge(H, I);
        H.addEdge(edge10);
        I.addEdge(edge10);

        Edge edge11 = new Edge(H, J);
        H.addEdge(edge11);
        J.addEdge(edge11);

        Edge edge12 = new Edge(I, J);
        I.addEdge(edge12);
        J.addEdge(edge12);

        nodes.add(A);
        nodes.add(B);
        nodes.add(C);
        nodes.add(D);
        nodes.add(E);
        nodes.add(F);
        nodes.add(G);
        nodes.add(H);
        nodes.add(I);
        nodes.add(J);


        /**
         Node A = new Node ("A", 1);
         Node B = new Node ("B", 1);
         Node C = new Node ("C", 1);
         Node D = new Node ("D", 1);

        Edge edge1 = new Edge(A, B);
        Edge edge2 = new Edge(A, C);
        Edge edge3 = new Edge(B, D);
        Edge edge4 = new Edge(C, D);

        A.addEdge(edge1);
        B.addEdge(edge1);

        A.addEdge(edge2);
        C.addEdge(edge2);

        B.addEdge(edge3);
        D.addEdge(edge3);

        C.addEdge(edge4);
        D.addEdge(edge4);

        nodes.add(A);
        nodes.add(B);
        nodes.add(C);
        nodes.add(D);
         */
    }

    private static void printResults(){
        System.out.println("Selected Locations: ");
        for(int i=0; i<selectedNodes.size(); i++){
            if(i==selectedNodes.size()-1){
                System.out.print(selectedNodes.get(i).getIdentifier());
            } else {
                System.out.print(selectedNodes.get(i).getIdentifier() +", ");
            }
        }
    }
}
