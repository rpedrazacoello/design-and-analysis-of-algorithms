package mx.rpedrazacoello.greedy.restaurant_location.util;

import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Objects;

public class Node implements Comparable{

    private String identifier;
    private int profit;
    private Hashtable<Edge, Edge> edges;
    private boolean active;

    /**
     * Constructor method
     * @param identifier
     */
    public Node(String identifier, int profit) {
        this.identifier = identifier;
        this.profit=profit;
        active = true;
        edges = new Hashtable<>();
    }

    /**
     * Method that adds an Edge to the node.
     * The edge is only added if node is contained in the edge.
     * @param edge
     */
    public void addEdge(Edge edge){
        if(edge.getNodeConnectedTo(this)!=null) {
            edges.put(edge, edge);
        }
    }

    /**
     * Returns the identifier of the node
     * @return
     */
    public String getIdentifier() {
        return identifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Node)) return false;
        Node node = (Node) o;
        return Objects.equals(identifier, node.identifier);
    }

    /**
     * Method that returns the edges of the node
     * @return
     */
    public Collection<Edge> getEdges(){
        return edges.values();
    }

    public int getProfit() {
        return profit;
    }

    @Override
    public int compareTo(Object o) {
        Node node = (Node) o;
        if(this.profit<node.profit){
            return 1;
        } else if (this.profit>node.profit){
            return -1;
        } else {
            return 0;
        }
    }

    public void deactivateNeighbors(){
        Iterator iterator = edges.elements().asIterator();
        while (iterator.hasNext()){
            Edge edge = (Edge) iterator.next();
            edge.getNodeConnectedTo(this).setActive(false);
        }
    }

    public void setActive (boolean active){
        this.active=active;
    }

    public boolean isActive(){
        return this.active;
    }
}
