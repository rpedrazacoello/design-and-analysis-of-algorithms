package mx.rpedrazacoello.greedy.IntervalScheduling;

public class Request implements Comparable{

    private int startTime;
    private int finishTime;

    public Request(int startTime, int finishTime) {
        this.startTime = startTime;
        this.finishTime = finishTime;
    }

    @Override
    public int compareTo(Object o) {
        Request request = (Request) o;
        if(this.finishTime<request.finishTime){
            return -1;
        } else if (this.finishTime>request.finishTime){
            return 1;
        } else {
            return 0;
        }
    }

    public int getStartTime() {
        return startTime;
    }

    public int getFinishTime() {
        return finishTime;
    }

    @Override
    public String toString() {
        return "Request{" +
                "startTime=" + startTime +
                ", finishTime=" + finishTime +
                '}';
    }
}
