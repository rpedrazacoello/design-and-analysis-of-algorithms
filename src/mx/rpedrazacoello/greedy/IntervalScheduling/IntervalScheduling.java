package mx.rpedrazacoello.greedy.IntervalScheduling;

import java.util.ArrayList;
import java.util.PriorityQueue;

/**
 * LECTURE VIDEO:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-046j-design-and-analysis-of-algorithms-spring-2015/lecture-videos/lecture-1-course-overview-interval-scheduling/
 *
 * LECTURE NOTES:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-046j-design-and-analysis-of-algorithms-spring-2015/lecture-notes/MIT6_046JS15_lec01.pdf
 *
 * Requests 1, 2, . . . , n, single resource
 * s(i) start time, f(i) finish time, s(i) < f(i) (start time must be less than finish time for a request)
 * Two requests i and j are compatible if they don’t overlap, i.e., f(i) ≤ s(j) or f(j) ≤ s(i).
 *
 * Goal: Select a compatible subset of requests of maximum size.
 */
public class IntervalScheduling {

    private static ArrayList <Request> requests;
    private static PriorityQueue <Request> minHeap;
    private static ArrayList <Request> maxSizeRequests;

    public static void main (String [] args){
        maxSizeRequests = new ArrayList<>();
        fillPriorityQueue();
        intervalScheduling();
        printResult();
    }

    /**
     * Select request with minimum finish time
     */
    private static void intervalScheduling(){
        Request currentRequest = minHeap.poll();
        if(minHeap.peek()!=null){
            maxSizeRequests.add(currentRequest);
        }

        while(minHeap.peek()!=null){
            Request lastRequestAdded = maxSizeRequests.get(maxSizeRequests.size()-1);
            currentRequest = minHeap.poll();
            if(lastRequestAdded.getFinishTime()<=currentRequest.getStartTime()){
                maxSizeRequests.add(currentRequest);
            }
        }
    }

    private static void printResult(){
        System.out.println("\nSubset of requests of maximum size: ");
        for(int i=0; i<maxSizeRequests.size(); i++){
            System.out.println(maxSizeRequests.get(i).toString());
        }
    }

    private static void fillPriorityQueue(){
        requests = new ArrayList<>();
        requests.add(new Request(1, 3));
        requests.add(new Request(3, 5));
        requests.add(new Request(6, 8));
        requests.add(new Request(5, 9));
        requests.add(new Request(5, 7));
        requests.add(new Request(8, 10));
        requests.add(new Request(8, 11));
        minHeap = new PriorityQueue<>(requests);
    }
}
