package mx.rpedrazacoello.dynamic_programming.interval_scheduling;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * LECTURE VIDEO:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-046j-design-and-analysis-of-algorithms-spring-2015/lecture-videos/lecture-1-course-overview-interval-scheduling/
 *
 * LECTURE NOTES:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-046j-design-and-analysis-of-algorithms-spring-2015/lecture-notes/MIT6_046JS15_lec01.pdf
 *
 * Requests 1, 2, . . . , n, single resource
 * s(i) start time, f(i) finish time, s(i) < f(i) (start time must be less than finish time for a request)
 * Two requests i and j are compatible if they don’t overlap, i.e., f(i) ≤ s(j) or f(j) ≤ s(i).
 * Each request i has weight w(i).
 *
 * Goal: Schedule subset of requests that are non-overlapping with maximum weight.
 */
public class IntervalScheduling {

    private static Hashtable<Integer, Integer> memoization;
    private static ArrayList<Request> requests;
    private static Hashtable<Integer, Integer> parentPointers;
    private static int max = Integer.MIN_VALUE;
    private static int firstRequest = -1;

    public static void main (String [] args){
        memoization = new Hashtable<>();
        parentPointers = new Hashtable<>();
        fillRequests();
        intervalScheduling(0);
        printResults(firstRequest);
    }

    /**
     * Dynamic programming algorithm
     * @param current
     * @return max weight starting at the current request
     */
    private static int intervalScheduling(int current){
        if(memoization.containsKey(current)){
            return memoization.get(current); //Memoization
        }

        //Base Case
        if(current==requests.size()-1){
            int weight = requests.get(current).getWeight();
            memoization.put(current, weight);
            parentPointers.put(current, current);

            if(weight>max){
                max = weight;
                firstRequest = current;
            }

            return memoization.get(current);
        }

        for(int i=current+1; i<requests.size(); i++){
            boolean compatible = false;
            int weight = requests.get(current).getWeight();
            int DPWeight = intervalScheduling(i);

            if(requests.get(current).getFinishTime()<=requests.get(i).getStartTime()) {
                weight = weight + DPWeight;
                compatible = true;
            }

            if(!memoization.containsKey(current) || memoization.get(current)<weight){
                memoization.put(current, weight);//Memoization
                if(compatible) {
                    parentPointers.put(current, i);
                } else {
                    parentPointers.put(current, current);
                }

                if(weight>max){
                    max = weight;
                    firstRequest = current;
                }

            }
        }

        return memoization.get(current);
    }

    /**
     * Print results
     */
    private static void printResults(int firstRequest){
        System.out.println("Max Weight: " +memoization.get(firstRequest));
        System.out.println("Subsets of requests of maximum weight: ");

        int current = firstRequest;
        int next = parentPointers.get(current);
        System.out.println(requests.get(current));
        while(current!=next){
            System.out.println(requests.get(next));
            current = next;
            next = parentPointers.get(current);
        }
    }

    private static void fillRequests(){
        requests = new ArrayList<>();
        requests.add(new Request(1, 3, 5));
        requests.add(new Request(3, 5, 6));
        requests.add(new Request(6, 8, 2));
        requests.add(new Request(1, 5, 12));
        //requests.add(new Request(5, 11, 13));
        requests.add(new Request(5, 9, 1));
        requests.add(new Request(5, 7, 3));
        requests.add(new Request(8, 10, 4));
        requests.add(new Request(8, 11, 9));
        requests.add(new Request(1, 11, 24));
        requests.sort((o1, o2) -> {
            if(o1.getFinishTime()<o2.getFinishTime()){
                return -1;
            } else if (o1.getFinishTime()>o2.getFinishTime()){
                return 1;
            } else {
                return 0;
            }
        });
    }
}
