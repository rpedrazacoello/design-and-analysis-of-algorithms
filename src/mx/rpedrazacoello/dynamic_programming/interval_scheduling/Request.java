package mx.rpedrazacoello.dynamic_programming.interval_scheduling;

public class Request implements Comparable{

    private int startTime;
    private int finishTime;
    private int weight;

    public Request(int startTime, int finishTime, int weight) {
        this.startTime = startTime;
        this.finishTime = finishTime;
        this.weight = weight;
    }

    public int getStartTime() {
        return startTime;
    }

    public int getFinishTime() {
        return finishTime;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public int compareTo(Object o) {
        Request request = (Request) o;
        if(this.finishTime<request.finishTime){
            return -1;
        } else if (this.finishTime>request.finishTime){
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return "Request{" +
                "startTime=" + startTime +
                ", finishTime=" + finishTime +
                ", weight=" + weight +
                '}';
    }
}
