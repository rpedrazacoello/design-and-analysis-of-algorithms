package mx.rpedrazacoello.dynamic_programming.restaurant_location;

import jdk.swing.interop.SwingInterOpUtils;
import mx.rpedrazacoello.greedy.restaurant_location.util.Edge;
import mx.rpedrazacoello.greedy.restaurant_location.util.Node;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * PROBLEM OBTAINED FROM:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-046j-design-and-analysis-of-algorithms-spring-2015/assignments/MIT6_046JS15_pset1.pdf
 *
 * Drunken Donuts, a new wine-and-donuts restaurant chain, wants to build restaurants on many street corners with the
 * goal of maximizing their total profit. The street network is described as an undirected graph G = (V, E), where the
 * potential restaurant sites are the vertices of the graph. Each vertex u has a nonnegative integer value pu, which
 * describes the potential profit of site u. Two restaurants cannot be built on adjacent vertices (to avoid
 * selfcompetition). You are supposed to design an algorithm that outputs the chosen set U ⊆ V of sites that maximizes
 * the total profit u∈U sum(pu).
 *
 * First, for parts (1)–(3), suppose that the street network G is acyclic, i.e., a tree.
 *
 * 2. Give an efficient algorithm to determine a placement with maximum profit
 * 4. Now suppose that the graph is arbitrary, not necessarily acyclic. Give the fastest correct algorithm you can for
 * solving the problem
 */
public class RestaurantLocation {
    private static ArrayList<Node> nodes;
    private static Hashtable<String, Integer> memoization;
    private static Hashtable<String, String> parentPointers;
    private static int maxValue=0;
    private static String maxIdentifier;

    public static void main (String [] args){
        nodes = new ArrayList<>();
        memoization = new Hashtable<>();
        parentPointers = new Hashtable<>();
        fill();
        nodes.get(0).setActive(false);
        selectLocations(nodes.get(0));
        printResult();
    }

    private static int selectLocations(Node startNode){
        if(memoization.containsKey(startNode.getIdentifier())){
            return memoization.get(startNode.getIdentifier()); //Memoization
        }

        ArrayList <Node> neighborNodes = new ArrayList<>(); //All nodes 1 edge away from startNode
        ArrayList <Node> grandparentNodes = new ArrayList<>(); //All nodes 2 edges away from startNode
        ArrayList<Node> activeNode = new ArrayList<>(); //Nodes that were unactivated during one recursion

        findNeighbors_Grandparents(startNode, neighborNodes, grandparentNodes);

        /**
         * Deactivate all the nodes that are neighbors of startNode
         */
        for(Node node: neighborNodes){
            if(node.isActive()) {
                node.setActive(false);
                selectLocations(node); //Find the maximum profit of all the neighbors of the startNode
                activeNode.add(node);
            }
        }

        int max = startNode.getProfit();
        String parent = startNode.getIdentifier();

        /**
         * Look for the max value possible by taking into account the startNode
         * The neighbors of startNode are still unactive, so there's no way they will be taken into
         * account in the choice
         */
        for(Node node: grandparentNodes){
            if(node.isActive()) {
                node.setActive(false);
                int tempMax = selectLocations(node) + startNode.getProfit(); //Find maximum profit for the grandparent Nodes
                if (tempMax > max) {
                    max = tempMax;
                    parent = node.getIdentifier();
                }
                node.setActive(true);
            }
        }

        //Reactivates the nodes that were deactivated
        for(Node node: activeNode) {
            node.setActive(true);
        }

        if(max >maxValue){
            maxValue=max;
            maxIdentifier = startNode.getIdentifier();
        }

        if(!memoization.containsKey(startNode.getIdentifier()) || memoization.get(startNode.getIdentifier())<max) {
            memoization.put(startNode.getIdentifier(), max); //Memoization
            parentPointers.put(startNode.getIdentifier(), parent);
        }

        return memoization.get(startNode.getIdentifier());
    }

    private static void printResult() {
        System.out.println("Max Profit: " +maxValue +"\n");
        String currentIdentifier = maxIdentifier;
        System.out.println("Selected locations: ");
        while(!parentPointers.get(currentIdentifier).equals(currentIdentifier)){
            System.out.print(currentIdentifier +", ");
            currentIdentifier = parentPointers.get(currentIdentifier);
        }
        System.out.print(parentPointers.get(currentIdentifier));
        System.out.println();
    }

    private static void fill(){
        Node A = new Node ("A", 1);
        Node B = new Node ("B", 1);
        Node C = new Node ("C", 1);
        Node D = new Node ("D", 1);
        Node E = new Node ("E", 1);
        Node F = new Node ("F", 1);
        Node G = new Node ("G", 1);
        Node H = new Node ("H", 1);
        Node I = new Node ("I", 1);
        Node J = new Node ("J", 1);

        Edge edge1 = new Edge(A, C);
        A.addEdge(edge1);
        C.addEdge(edge1);

        Edge edge2 = new Edge(B, C);
        B.addEdge(edge2);
        C.addEdge(edge2);

        Edge edge3 = new Edge(B, D);
        B.addEdge(edge3);
        D.addEdge(edge3);

        Edge edge4 = new Edge(C, D);
        C.addEdge(edge4);
        D.addEdge(edge4);

        Edge edge5 = new Edge(D, E);
        D.addEdge(edge5);
        E.addEdge(edge5);

        Edge edge6 = new Edge(E, F);
        E.addEdge(edge6);
        F.addEdge(edge6);

        Edge edge7 = new Edge(E, G);
        E.addEdge(edge7);
        G.addEdge(edge7);

        Edge edge8 = new Edge(F, G);
        F.addEdge(edge8);
        G.addEdge(edge8);

        Edge edge9 = new Edge(G, J);
        G.addEdge(edge9);
        J.addEdge(edge9);

        Edge edge10 = new Edge(H, I);
        H.addEdge(edge10);
        I.addEdge(edge10);

        Edge edge11 = new Edge(H, J);
        H.addEdge(edge11);
        J.addEdge(edge11);

        Edge edge12 = new Edge(I, J);
        I.addEdge(edge12);
        J.addEdge(edge12);

        nodes.add(A);
        nodes.add(B);
        nodes.add(C);
        nodes.add(D);
        nodes.add(E);
        nodes.add(F);
        nodes.add(G);
        nodes.add(H);
        nodes.add(I);
        nodes.add(J);

        /*
        Node A = new Node("A", 1);
        Node B = new Node ("B", 1);
        Node C = new Node ("C", 1);
        Node D = new Node ("D", 1);

        Edge edge1 = new Edge(A, B);
        Edge edge2 = new Edge(A, C);
        Edge edge3 = new Edge(B, D);
        Edge edge4 = new Edge(C, D);

        A.addEdge(edge1);
        B.addEdge(edge1);

        A.addEdge(edge2);
        C.addEdge(edge2);

        B.addEdge(edge3);
        D.addEdge(edge3);

        C.addEdge(edge4);
        D.addEdge(edge4);

        nodes.add(A);
        nodes.add(B);
        nodes.add(C);
        nodes.add(D);
        */

    }

    private static void findNeighbors_Grandparents(Node startNode, ArrayList neighborNodes, ArrayList grandparentNodes){
        for (Edge edge: startNode.getEdges()) {
            Node nodeConnected = edge.getNodeConnectedTo(startNode);

            if (nodeConnected.isActive()) {
                neighborNodes.add(nodeConnected); //Find all neighbors of startNode

                for (Edge grandparentEdge : nodeConnected.getEdges()){
                    Node grandparentNode = grandparentEdge.getNodeConnectedTo(nodeConnected);
                    if(grandparentNode.isActive()){
                        grandparentNodes.add(grandparentNode); //Find all the neighbors of the neighbors of startNode
                        // (grandparents nodes) because the 2 edges distance
                    }
                }
            }
        }
    }


}
